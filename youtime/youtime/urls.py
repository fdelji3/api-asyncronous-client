from django.contrib import admin
from django.urls import path
from gestor.views import ApiClient
from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from gestor.consumers import ApiConsumer

urlpatterns = [
    path('admin/',admin.site.urls),
    path('',ApiClient.as_view(name='home')),
    url(r'^order_offer$', ApiConsumer)
]+static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
