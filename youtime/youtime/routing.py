# mysite/routing.py
from django.conf.urls import url
from channels.auth import AuthMiddlewareStack
from channels.routing import ProtocolTypeRouter, URLRouter
import gestor.routing
from channels.security.websocket import AllowedHostsOriginValidator,OriginValidator

application = ProtocolTypeRouter({
    # (http->django views is added by default)
    'websocket': AllowedHostsOriginValidator(
        AuthMiddlewareStack(
            URLRouter(
                gestor.routing.websocket_urlpatterns
            )         
        )
    ),
})
