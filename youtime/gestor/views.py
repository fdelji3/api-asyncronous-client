from django.contrib.auth.models import User, AnonymousUser
from django.views import View
from django.shortcuts import render, redirect
from django.utils.decorators import method_decorator
from django.views.decorators.csrf import csrf_exempt
from datetime import datetime
from .models import Task
import json
import requests
import hashlib
import os
import sys
from .consumers import ApiConsumer
from channels.db import database_sync_to_async
from django.core import serializers
# Vista Home de la APP (Pagina de inicio con toda la info y diario)


class ApiClient(View):
    name = 'home'
    @method_decorator(csrf_exempt)
    def dispatch(self, request, *args, **kwargs):
        return super(ApiClient, self).dispatch(request, *args, **kwargs)

    def get(self, request, *args, **kwargs):
        tareas = Task.objects.all()
        return render(request, 'home.html',{'resultados':tareas})

    def post(self, request, *args, **kwargs):
        form = json.loads(request.body)
        tareas = Task.objects.all()
        username = form['username']
        password = form['password']
        nueva_tarea = Task(username=username, password=password)
        nueva_tarea.save()
        return render(request, 'home.html',{'resultados':tareas})

        with open("media/data_file.txt", "a+") as f:
            f.write(str('\n'))
            f.write(str('La tarea '))
            f.write(str(form['username']))
            now = datetime.now()
            time = now.strftime("%H:%M:%S")
            f.write(str(' ha terminado a las '))
            f.write(str(time))
            archivo = open('media/data_file.txt', 'r')
            objeto = archivo.readlines()
            return render(request, 'home.html')

